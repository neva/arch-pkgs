#!/usr/bin/env bash

cat */PKGBUILD | grep -e "    '" | awk '{$1=$1};1' | sort | uniq -d
